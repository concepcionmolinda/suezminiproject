package sg.gov.ntp.vo.jpa;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ClassroomEntityFactoryForTest {

  private static MockValues mockValues = new MockValues();

  public ClassroomEntity newClassroomEntity() {

    BigDecimal classroomId = mockValues.nextBigDecimal();

    ClassroomEntity classroomEntity = new ClassroomEntity();
    classroomEntity.setClassroomId(classroomId);
    return classroomEntity;
  }

  private static ClassroomEntity classroomEntityTestValue = null;

  private static BigDecimal classroomIdEntityInternalMockValue = mockValues
      .nextBigDecimal();
  private static BigDecimal classroomNoEntityInternalMockValue = mockValues
      .nextBigDecimal();

  @Before
  public void setUp() throws Exception {

  }

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    classroomEntityTestValue = new ClassroomEntity();

    // Prepare test data for Getter methods.
    classroomEntityTestValue.setClassroomId(classroomIdEntityInternalMockValue);
    classroomEntityTestValue.setClassroomNo(classroomNoEntityInternalMockValue);
  }

  @After
  public void tearDown() throws Exception {

  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    classroomEntityTestValue = null;
    mockValues = null;
  }

  @Test
  public void testToString() {
    assertEquals(classroomEntityTestValue.toString(),
        "[" + classroomIdEntityInternalMockValue + "]:" + classroomNoEntityInternalMockValue);
  }

}
