/*
 * Created on 11 Sep 2017 ( Time 16:40:42 )
 */
package sg.gov.ntp.business.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import sg.gov.ntp.business.service.impl.StudentServiceImpl;
import sg.gov.ntp.business.service.mapping.StudentServiceMapper;
import sg.gov.ntp.data.repository.jpa.StudentJpaRepository;
import sg.gov.ntp.vo.StudentVO;
import sg.gov.ntp.vo.jpa.MockValues;
import sg.gov.ntp.vo.jpa.StudentEntity;
import sg.gov.ntp.vo.jpa.StudentEntityFactoryForTest;
import sg.gov.ntp.vo.jpa.StudentFactoryForTest;

/**
 * Test : Implementation of StudentService
 */
@RunWith(MockitoJUnitRunner.class)
public class StudentServiceImplTest {

	@InjectMocks
	private StudentServiceImpl studentService;
	@Mock
	private StudentJpaRepository studentJpaRepository;
	@Mock
	private StudentServiceMapper studentServiceMapper;

	private StudentFactoryForTest studentFactoryForTest = new StudentFactoryForTest();

	private StudentEntityFactoryForTest studentEntityFactoryForTest = new StudentEntityFactoryForTest();

	private MockValues mockValues = new MockValues();

	@Test
	public void findById() {
		// Given
		BigDecimal studentId = mockValues.nextBigDecimal();

		StudentEntity studentEntity = studentJpaRepository.findOne(studentId);

		StudentVO student = studentFactoryForTest.newStudent();
		when(studentServiceMapper.mapStudentEntityToStudent(studentEntity)).thenReturn(student);

		// When
		StudentVO studentFound = studentService.findById(studentId);

		// Then
		assertEquals(student.getStudentId(),studentFound.getStudentId());
	}

	@Test
	public void findAll() {
		// Given
		List<StudentEntity> studentEntitys = new ArrayList<StudentEntity>();
		StudentEntity studentEntity1 = studentEntityFactoryForTest.newStudentEntity();
		studentEntitys.add(studentEntity1);
		StudentEntity studentEntity2 = studentEntityFactoryForTest.newStudentEntity();
		studentEntitys.add(studentEntity2);
		when(studentJpaRepository.findAll()).thenReturn(studentEntitys);

		StudentVO student1 = studentFactoryForTest.newStudent();
		when(studentServiceMapper.mapStudentEntityToStudent(studentEntity1)).thenReturn(student1);
		StudentVO student2 = studentFactoryForTest.newStudent();
		when(studentServiceMapper.mapStudentEntityToStudent(studentEntity2)).thenReturn(student2);

		// When
		List<StudentVO> studentsFounds = studentService.findAll();

		// Then
		assertTrue(student1 == studentsFounds.get(0));
		assertTrue(student2 == studentsFounds.get(1));
	}

	@Test
	public void create() {
		// Given
		StudentVO student = studentFactoryForTest.newStudent();

		StudentEntity studentEntity = studentEntityFactoryForTest.newStudentEntity();
		when(studentJpaRepository.findOne(student.getStudentId())).thenReturn(null);

		studentEntity = new StudentEntity();
		studentServiceMapper.mapStudentToStudentEntity(student, studentEntity);
		StudentEntity studentEntitySaved = studentJpaRepository.save(studentEntity);

		StudentVO studentSaved = studentFactoryForTest.newStudent();
		when(studentServiceMapper.mapStudentEntityToStudent(studentEntitySaved)).thenReturn(studentSaved);

		// When
		StudentVO studentResult = studentService.create(student);

		// Then
		assertTrue(studentResult == studentSaved);
	}

	@Test
	public void createKOExists() {
		// Given
		StudentVO student = studentFactoryForTest.newStudent();

		StudentEntity studentEntity = studentEntityFactoryForTest.newStudentEntity();
		when(studentJpaRepository.findOne(student.getStudentId())).thenReturn(studentEntity);

		// When
		Exception exception = null;
		try {
			studentService.create(student);
		} catch(Exception e) {
			exception = e;
		}

		// Then
		assertTrue(exception instanceof IllegalStateException);
		assertEquals("already.exists", exception.getMessage());
	}

	@Test
	public void update() {
		// Given
		StudentVO student = studentFactoryForTest.newStudent();

		StudentEntity studentEntity = studentEntityFactoryForTest.newStudentEntity();
		when(studentJpaRepository.findOne(student.getStudentId())).thenReturn(studentEntity);

		StudentEntity studentEntitySaved = studentEntityFactoryForTest.newStudentEntity();
		when(studentJpaRepository.save(studentEntity)).thenReturn(studentEntitySaved);

		StudentVO studentSaved = studentFactoryForTest.newStudent();
		when(studentServiceMapper.mapStudentEntityToStudent(studentEntitySaved)).thenReturn(studentSaved);

		// When
		StudentVO studentResult = studentService.update(student);

		// Then
		verify(studentServiceMapper).mapStudentToStudentEntity(student, studentEntity);
		assertTrue(studentResult == studentSaved);
	}

	@Test
	public void delete() {
		// Given
		BigDecimal studentId = mockValues.nextBigDecimal();

		// When
		studentService.delete(studentId);

		// Then
		verify(studentJpaRepository).delete(studentId);

	}

}
