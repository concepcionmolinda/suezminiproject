package sg.gov.ntp.vo.jpa;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StudentEntityFactoryForTest {

  private static MockValues mockValues = new MockValues();

  public StudentEntity newStudentEntity() {

    BigDecimal studentId = mockValues.nextBigDecimal();

    StudentEntity studentEntity = new StudentEntity();
    studentEntity.setStudentId(studentId);

    return studentEntity;
  }

  private static StudentEntity studentEntityTestValue = null;

  private static BigDecimal studentIdEntityInternalMockValue = mockValues
      .nextBigDecimal();
  private static String fnameEntityInternalMockValue = mockValues
      .nextString(50);
  private static String lnameEntityInternalMockValue = mockValues
      .nextString(50);
  private static BigDecimal ageEntityInternalMockValue = mockValues
      .nextBigDecimal();
  private static String genderEntityInternalMockValue = mockValues
      .nextString(2);
  private static String createdByEntityInternalMockValue = mockValues
      .nextString(20);
  private static Date createdDtEntityInternalMockValue = mockValues.nextDate();
  private static Date lastUpdatedDtEntityInternalMockValue = mockValues
      .nextDate();
  private static String lastUpdatedByEntityInternalMockValue = mockValues
      .nextString(20);
  private static String serverNameEntityInternalMockValue = mockValues
      .nextString(20);
  private static String systemNameEntityInternalMockValue = mockValues
      .nextString(20);
  private static BigDecimal verNoEntityInternalMockValue = mockValues
      .nextBigDecimal();

  @Before
  public void setUp() throws Exception {

  }

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    studentEntityTestValue = new StudentEntity();

    // Prepare test data for Getter methods.
    studentEntityTestValue.setStudentId(studentIdEntityInternalMockValue);
    studentEntityTestValue.setFname(fnameEntityInternalMockValue);
    studentEntityTestValue.setLname(lnameEntityInternalMockValue);
    studentEntityTestValue.setAge(ageEntityInternalMockValue);
    studentEntityTestValue.setGender(genderEntityInternalMockValue);
    studentEntityTestValue.setCreatedBy(createdByEntityInternalMockValue);
    studentEntityTestValue.setCreatedDt(createdDtEntityInternalMockValue);
    studentEntityTestValue
        .setLastUpdatedDt(lastUpdatedDtEntityInternalMockValue);
    studentEntityTestValue
        .setLastUpdatedBy(lastUpdatedByEntityInternalMockValue);
    studentEntityTestValue.setServerName(serverNameEntityInternalMockValue);
    studentEntityTestValue.setSystemName(systemNameEntityInternalMockValue);
    // studentTestValue.setVerNo(verNoInternalMockValue);
  }

  @After
  public void tearDown() throws Exception {

  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    studentEntityTestValue = null;
    mockValues = null;
  }

  @Test
  public void testToString() {
    assertEquals(studentEntityTestValue.toString(),
        "[" + studentIdEntityInternalMockValue + "]:"
            + fnameEntityInternalMockValue + "|" + lnameEntityInternalMockValue
            + "|" + ageEntityInternalMockValue + "|"
            + genderEntityInternalMockValue + "|"
            + createdByEntityInternalMockValue + "|"
            + createdDtEntityInternalMockValue + "|"
            + lastUpdatedDtEntityInternalMockValue + "|"
            + lastUpdatedByEntityInternalMockValue + "|"
            + serverNameEntityInternalMockValue + "|"
            + systemNameEntityInternalMockValue + "|" + null);
  }

}
