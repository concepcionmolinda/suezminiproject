/*
 * Created on 11 Sep 2017 ( Time 16:40:42 )
 */
package sg.gov.ntp.business.service.mapping;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import sg.gov.ntp.vo.StudentVO;
import sg.gov.ntp.vo.jpa.MockValues;
import sg.gov.ntp.vo.jpa.StudentEntity;

/**
 * Test : Mapping between entity beans and display beans.
 */
public class StudentServiceMapperTest {

	private StudentServiceMapper studentServiceMapper;

	private static ModelMapper modelMapper = new ModelMapper();

	private MockValues mockValues = new MockValues();


	@BeforeClass
	public static void setUp() {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	@Before
	public void before() {
		studentServiceMapper = new StudentServiceMapper();
		studentServiceMapper.setModelMapper(modelMapper);
	}

	/**
	 * Mapping from 'StudentEntity' to 'Student'
	 * @param studentEntity
	 */
	@Test
	public void testMapStudentEntityToStudent() {
		// Given
		StudentEntity studentEntity = new StudentEntity();
		studentEntity.setFname(mockValues.nextString(50));
		studentEntity.setLname(mockValues.nextString(50));
		studentEntity.setAge(mockValues.nextBigDecimal());
		studentEntity.setGender(mockValues.nextString(2));
		studentEntity.setCreatedBy(mockValues.nextString(20));
		studentEntity.setCreatedDt(mockValues.nextDate());
		studentEntity.setLastUpdatedDt(mockValues.nextDate());
		studentEntity.setLastUpdatedBy(mockValues.nextString(20));
		studentEntity.setServerName(mockValues.nextString(20));
		studentEntity.setSystemName(mockValues.nextString(20));
		studentEntity.setVerNo(mockValues.nextLong());

		// When
		StudentVO student = studentServiceMapper.mapStudentEntityToStudent(studentEntity);

		// Then
		assertEquals(studentEntity.getFname(), student.getFname());
		assertEquals(studentEntity.getLname(), student.getLname());
		assertEquals(studentEntity.getAge(), student.getAge());
		assertEquals(studentEntity.getGender(), student.getGender());
		assertEquals(studentEntity.getCreatedBy(), student.getCreatedBy());
		assertEquals(studentEntity.getCreatedDt(), student.getCreatedDt());
		assertEquals(studentEntity.getLastUpdatedDt(), student.getLastUpdatedDt());
		assertEquals(studentEntity.getLastUpdatedBy(), student.getLastUpdatedBy());
		assertEquals(studentEntity.getServerName(), student.getServerName());
		assertEquals(studentEntity.getSystemName(), student.getSystemName());
		assertEquals(studentEntity.getVerNo(), student.getVerNo());
	}

	/**
	 * Test : Mapping from 'Student' to 'StudentEntity'
	 */
	@Test
	public void testMapStudentToStudentEntity() {
		// Given
		StudentVO student = new StudentVO();
		student.setFname(mockValues.nextString(50));
		student.setLname(mockValues.nextString(50));
		student.setAge(mockValues.nextBigDecimal());
		student.setGender(mockValues.nextString(2));
		student.setCreatedBy(mockValues.nextString(20));
		student.setCreatedDt(mockValues.nextDate());
		student.setLastUpdatedDt(mockValues.nextDate());
		student.setLastUpdatedBy(mockValues.nextString(20));
		student.setServerName(mockValues.nextString(20));
		student.setSystemName(mockValues.nextString(20));
		student.setVerNo(mockValues.nextLong());

		StudentEntity studentEntity = new StudentEntity();

		// When
		studentServiceMapper.mapStudentToStudentEntity(student, studentEntity);

		// Then
		assertEquals(student.getFname(), studentEntity.getFname());
		assertEquals(student.getLname(), studentEntity.getLname());
		assertEquals(student.getAge(), studentEntity.getAge());
		assertEquals(student.getGender(), studentEntity.getGender());
		assertEquals(student.getCreatedBy(), studentEntity.getCreatedBy());
		assertEquals(student.getCreatedDt(), studentEntity.getCreatedDt());
		assertEquals(student.getLastUpdatedDt(), studentEntity.getLastUpdatedDt());
		assertEquals(student.getLastUpdatedBy(), studentEntity.getLastUpdatedBy());
		assertEquals(student.getServerName(), studentEntity.getServerName());
		assertEquals(student.getSystemName(), studentEntity.getSystemName());
		assertEquals(student.getVerNo(), studentEntity.getVerNo());
	}

}