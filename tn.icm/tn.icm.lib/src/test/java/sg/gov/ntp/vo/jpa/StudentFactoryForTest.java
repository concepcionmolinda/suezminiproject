package sg.gov.ntp.vo.jpa;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sg.gov.ntp.vo.StudentVO;

public class StudentFactoryForTest {

  private static MockValues mockValues = new MockValues();

  public StudentVO newStudent() {

    BigDecimal studentId = mockValues.nextBigDecimal();

    StudentVO student = new StudentVO();
    student.setStudentId(studentId);

    return student;
  }

  private static StudentVO studentTestValue = null;

  private static BigDecimal studentIdInternalMockValue = mockValues
      .nextBigDecimal();
  private static String fnameInternalMockValue = mockValues.nextString(50);
  private static String lnameInternalMockValue = mockValues.nextString(50);
  private static BigDecimal ageInternalMockValue = mockValues.nextBigDecimal();
  private static String genderInternalMockValue = mockValues.nextString(2);
  private static String createdByInternalMockValue = mockValues.nextString(20);
  private static Date createdDtInternalMockValue = mockValues.nextDate();
  private static Date lastUpdatedDtInternalMockValue = mockValues.nextDate();
  private static String lastUpdatedByInternalMockValue = mockValues
      .nextString(20);
  private static String serverNameInternalMockValue = mockValues.nextString(20);
  private static String systemNameInternalMockValue = mockValues.nextString(20);
  private static BigDecimal verNoInternalMockValue = mockValues
      .nextBigDecimal();

  @Before
  public void setUp() throws Exception {

  }

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    studentTestValue = new StudentVO();

    // Prepare test data for Getter methods.
    studentTestValue.setStudentId(studentIdInternalMockValue);
    studentTestValue.setFname(fnameInternalMockValue);
    studentTestValue.setLname(lnameInternalMockValue);
    studentTestValue.setAge(ageInternalMockValue);
    studentTestValue.setGender(genderInternalMockValue);
    studentTestValue.setCreatedBy(createdByInternalMockValue);
    studentTestValue.setCreatedDt(createdDtInternalMockValue);
    studentTestValue.setLastUpdatedDt(lastUpdatedDtInternalMockValue);
    studentTestValue.setLastUpdatedBy(lastUpdatedByInternalMockValue);
    studentTestValue.setServerName(serverNameInternalMockValue);
    studentTestValue.setSystemName(systemNameInternalMockValue);
    // studentTestValue.setVerNo(verNoInternalMockValue);
  }

  @After
  public void tearDown() throws Exception {

  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    studentTestValue = null;
    mockValues = null;
  }

  @Test
  public void testToString() {
    assertEquals(studentTestValue.toString(),
        studentIdInternalMockValue + "|" + fnameInternalMockValue + "|"
            + lnameInternalMockValue + "|" + ageInternalMockValue + "|"
            + genderInternalMockValue + "|" + createdByInternalMockValue + "|"
            + createdDtInternalMockValue + "|" + lastUpdatedDtInternalMockValue
            + "|" + lastUpdatedByInternalMockValue + "|"
            + serverNameInternalMockValue + "|" + systemNameInternalMockValue
            + "|" + null);
  }

}
