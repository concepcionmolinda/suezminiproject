/*
 * Created on 11 Sep 2017 ( Time 16:40:42 )
 */
package sg.gov.ntp.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import sg.gov.ntp.vo.Classroom;
import sg.gov.ntp.vo.jpa.ClassroomEntity;
import sg.gov.ntp.vo.jpa.StudentEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class ClassroomServiceMapper extends AbstractServiceMapper {

  /**
   * ModelMapper : bean to bean mapping library.
   */
  private ModelMapper modelMapper;

  /**
   * Constructor.
   */
  public ClassroomServiceMapper() {
    modelMapper = new ModelMapper();
    modelMapper.getConfiguration()
        .setMatchingStrategy(MatchingStrategies.STRICT);
  }

  /**
   * Mapping from 'ClassroomEntity' to 'Classroom'
   *
   * @param classroomEntity
   */
  public Classroom mapClassroomEntityToClassroom(
      ClassroomEntity classroomEntity) {

    // --- Generic mapping
    Classroom classroom = map(classroomEntity, Classroom.class);

    // --- Link mapping ( link to Student )
    classroom.setStudentId(classroomEntity.getStudentfk().getStudentId());
    return classroom;
  }

  /**
   * Mapping from 'Classroom' to 'ClassroomEntity'
   *
   * @param classroom
   * @param classroomEntity
   */
  public void mapClassroomToClassroomEntity(Classroom classroom,
      ClassroomEntity classroomEntity) {

    // --- Generic mapping
    map(classroom, classroomEntity);

    // --- Link mapping ( link : classroom )

    StudentEntity student1 = new StudentEntity();
    student1.setStudentId(classroom.getStudentId());
    classroomEntity.setStudentfk(student1);

  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected ModelMapper getModelMapper() {
    return modelMapper;
  }

  protected void setModelMapper(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

}