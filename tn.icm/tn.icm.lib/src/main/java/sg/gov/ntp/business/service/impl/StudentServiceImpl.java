/*
 * Created on 11 Sep 2017 ( Time 16:40:42 )
 */
package sg.gov.ntp.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sg.gov.ntp.business.service.StudentService;
import sg.gov.ntp.business.service.mapping.StudentServiceMapper;
import sg.gov.ntp.common.service.NTPBaseService;
import sg.gov.ntp.data.repository.jpa.StudentJpaRepository;
import sg.gov.ntp.vo.StudentVO;
import sg.gov.ntp.vo.jpa.StudentEntity;

/**
 * Implementation of StudentService
 */
@Component
@Transactional
public class StudentServiceImpl extends NTPBaseService implements StudentService {

  @Resource
  private StudentJpaRepository studentJpaRepository;

  @Resource
  private StudentServiceMapper studentServiceMapper;

  public StudentVO findById(BigDecimal studentId) {
    StudentEntity studentEntity = studentJpaRepository.findOne(studentId);
    return studentServiceMapper.mapStudentEntityToStudent(studentEntity);
  }

  public List<StudentVO> findAll() {
    Iterable<StudentEntity> entities = studentJpaRepository.findAll();
    List<StudentVO> beans = new ArrayList<StudentVO>();
    for (StudentEntity studentEntity : entities) {
      beans.add(studentServiceMapper.mapStudentEntityToStudent(studentEntity));
    }
    return beans;
  }

  public StudentVO save(StudentVO student) {
    return update(student);
  }

  public StudentVO create(StudentVO student) {
    StudentEntity studentEntity = studentJpaRepository
        .findOne(student.getStudentId());
    if (studentEntity != null) {
      throw new IllegalStateException("already.exists");
    }
    studentEntity = new StudentEntity();
    studentServiceMapper.mapStudentToStudentEntity(student, studentEntity);
    StudentEntity studentEntitySaved = studentJpaRepository.save(studentEntity);
    return studentServiceMapper.mapStudentEntityToStudent(studentEntitySaved);
  }

  public StudentVO update(StudentVO student) {
    StudentEntity studentEntity = studentJpaRepository
        .findOne(student.getStudentId());
    studentServiceMapper.mapStudentToStudentEntity(student, studentEntity);
    StudentEntity studentEntitySaved = studentJpaRepository.save(studentEntity);
    return studentServiceMapper.mapStudentEntityToStudent(studentEntitySaved);
  }

  public void delete(BigDecimal studentId) {
    studentJpaRepository.delete(studentId);
  }

  public StudentJpaRepository getStudentJpaRepository() {
    return studentJpaRepository;
  }

  public void setStudentJpaRepository(
      StudentJpaRepository studentJpaRepository) {
    this.studentJpaRepository = studentJpaRepository;
  }

  public StudentServiceMapper getStudentServiceMapper() {
    return studentServiceMapper;
  }

  public void setStudentServiceMapper(
      StudentServiceMapper studentServiceMapper) {
    this.studentServiceMapper = studentServiceMapper;
  }

}
