package sg.gov.ntp.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import sg.gov.ntp.vo.jpa.StudentEntity;

import java.math.BigDecimal;

/**
 * Repository : Student.
 */
public interface StudentJpaRepository extends PagingAndSortingRepository<StudentEntity, BigDecimal> {

}
