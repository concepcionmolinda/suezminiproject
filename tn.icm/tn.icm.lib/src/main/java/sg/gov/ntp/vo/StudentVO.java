/*
 * Created on 11 Sep 2017 ( Time 16:40:42 )
 */
package sg.gov.ntp.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StudentVO implements Serializable {

  private static final long serialVersionUID = 1L;

  // ----------------------------------------------------------------------
  // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
  // ----------------------------------------------------------------------
  @NotNull
  private BigDecimal studentId;

  // ----------------------------------------------------------------------
  // ENTITY DATA FIELDS
  // ----------------------------------------------------------------------
  @NotNull
  @Size(min = 1, max = 50)
  private String studFname;

  @NotNull
  @Size(min = 1, max = 50)
  private String studLname;

  @NotNull
  private BigDecimal studAge;

  @NotNull
  @Size(min = 1, max = 2)
  private String studGender;

  @Size(max = 20)
  private String studCreatedBy;

  private Date studCreatedDt;

  private Date studLastUpdatedDt;

  @Size(max = 20)
  private String studLastUpdatedBy;

  @Size(max = 20)
  private String studServerName;

  @Size(max = 20)
  private String studSystemName;

  private Long studVerNo;

  // ----------------------------------------------------------------------
  // GETTER & SETTER FOR THE KEY FIELD
  // ----------------------------------------------------------------------
  public void setStudentId(BigDecimal studentId) {
    this.studentId = studentId;
  }

  public BigDecimal getStudentId() {
    return this.studentId;
  }

  // ----------------------------------------------------------------------
  // GETTERS & SETTERS FOR FIELDS
  // ----------------------------------------------------------------------
  public void setFname(String fname) {
    this.studFname = fname;
  }

  public String getFname() {
    return this.studFname;
  }

  public void setLname(String lname) {
    this.studLname = lname;
  }

  public String getLname() {
    return this.studLname;
  }

  public void setAge(BigDecimal age) {
    this.studAge = age;
  }

  public BigDecimal getAge() {
    return this.studAge;
  }

  public void setGender(String gender) {
    this.studGender = gender;
  }

  public String getGender() {
    return this.studGender;
  }

  public void setCreatedBy(String createdBy) {
    this.studCreatedBy = createdBy;
  }

  public String getCreatedBy() {
    return this.studCreatedBy;
  }

  public void setCreatedDt(Date createdDt) {
    this.studCreatedDt = createdDt;
  }

  public Date getCreatedDt() {
    return this.studCreatedDt;
  }

  public void setLastUpdatedDt(Date lastUpdatedDt) {
    this.studLastUpdatedDt = lastUpdatedDt;
  }

  public Date getLastUpdatedDt() {
    return this.studLastUpdatedDt;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.studLastUpdatedBy = lastUpdatedBy;
  }

  public String getLastUpdatedBy() {
    return this.studLastUpdatedBy;
  }

  public void setServerName(String serverName) {
    this.studServerName = serverName;
  }

  public String getServerName() {
    return this.studServerName;
  }

  public void setSystemName(String systemName) {
    this.studSystemName = systemName;
  }

  public String getSystemName() {
    return this.studSystemName;
  }

  public void setVerNo(Long verNo) {
    this.studVerNo = verNo;
  }

  public Long getVerNo() {
    return this.studVerNo;
  }

  // ----------------------------------------------------------------------
  // toString METHOD
  // ----------------------------------------------------------------------

  @Override
  public String toString() {

    StringBuilder sb = new StringBuilder();

    sb.append(studentId);
    sb.append("|");
    sb.append(studFname);
    sb.append("|");
    sb.append(studLname);
    sb.append("|");
    sb.append(studAge);
    sb.append("|");
    sb.append(studGender);
    sb.append("|");
    sb.append(studCreatedBy);
    sb.append("|");
    sb.append(studCreatedDt);
    sb.append("|");
    sb.append(studLastUpdatedDt);
    sb.append("|");
    sb.append(studLastUpdatedBy);
    sb.append("|");
    sb.append(studServerName);
    sb.append("|");
    sb.append(studSystemName);
    sb.append("|");
    sb.append(studVerNo);

    return sb.toString();
  }

}
