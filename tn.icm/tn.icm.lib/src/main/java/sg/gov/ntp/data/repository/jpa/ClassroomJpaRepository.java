package sg.gov.ntp.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import sg.gov.ntp.vo.jpa.ClassroomEntity;

import java.math.BigDecimal;

/**
 * Repository : Classroom.
 */
public interface ClassroomJpaRepository
    extends PagingAndSortingRepository<ClassroomEntity, BigDecimal> {

}
