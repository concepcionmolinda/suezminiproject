/*
 * Created on 11 Sep 2017 ( Time 16:40:42 )
 */
package sg.gov.ntp.business.service;

import java.util.List;

import sg.gov.ntp.vo.StudentVO;

import java.math.BigDecimal;

/**
 * Business Service Interface for entity Student.
 */
public interface StudentService {

  /**
   * Loads an entity from the database using its Primary Key
   *
   * @param studentId
   * @return entity
   */
  StudentVO findById(BigDecimal studentId);

  /**
   * Loads all entities.
   *
   * @return all entities
   */
  List<StudentVO> findAll();

  /**
   * Saves the given entity in the database (create or update)
   *
   * @param entity
   * @return entity
   */
  StudentVO save(StudentVO entity);

  /**
   * Updates the given entity in the database
   *
   * @param entity
   * @return
   */
  StudentVO update(StudentVO entity);

  /**
   * Creates the given entity in the database
   *
   * @param entity
   * @return
   */
  StudentVO create(StudentVO entity);

  /**
   * Deletes an entity using its Primary Key
   *
   * @param studentId
   */
  void delete(BigDecimal studentId);

}
