/*
 * Created on 11 Sep 2017 ( Time 16:40:42 )
 */
// This Bean has a basic Primary Key (not composite)

package sg.gov.ntp.vo.jpa;

import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import sg.gov.ntp.common.data.NTPBaseEntity;

/**
 * Persistent class for entity stored in table "STUDENT"
 *
 *
 */

@Entity
@Table(name = "STUDENT", schema = "SYSTEM")
// Define named queries here
@NamedQueries({
    @NamedQuery(name = "StudentEntity.countAll", query = "SELECT COUNT(x) FROM StudentEntity x") })
public class StudentEntity extends NTPBaseEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  // ----------------------------------------------------------------------
  // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
  // ----------------------------------------------------------------------
  @Id
  @Column(name = "STUDENT_ID", nullable = false)
  private BigDecimal studentId;

  // ----------------------------------------------------------------------
  // ENTITY DATA FIELDS
  // ----------------------------------------------------------------------
  @Column(name = "FNAME", nullable = false, length = 50)
  private String fname;

  @Column(name = "LNAME", nullable = false, length = 50)
  private String lname;

  @Column(name = "AGE", nullable = false)
  private BigDecimal age;

  @Column(name = "GENDER", nullable = false, length = 2)
  private String gender;

  @Column(name = "CREATED_BY", length = 20)
  private String createdBy;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CREATED_DT")
  private Date createdDt;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "LAST_UPDATED_DT")
  private Date lastUpdatedDt;

  @Column(name = "LAST_UPDATED_BY", length = 20)
  private String lastUpdatedBy;

  @Column(name = "SERVER_NAME", length = 20)
  private String serverName;

  @Column(name = "SYSTEM_NAME", length = 20)
  private String systemName;

  @Column(name = "VER_NO")
  private Long verNo;

  // ----------------------------------------------------------------------
  // ENTITY LINKS ( RELATIONSHIP )
  // ----------------------------------------------------------------------
  @OneToMany(mappedBy = "studentfk", targetEntity = ClassroomEntity.class)
  private List<ClassroomEntity> listOfClassroomfk;

  // ----------------------------------------------------------------------
  // CONSTRUCTOR(S)
  // ----------------------------------------------------------------------
  public StudentEntity() {
    super();
  }

  // ----------------------------------------------------------------------
  // GETTER & SETTER FOR THE KEY FIELD
  // ----------------------------------------------------------------------
  public void setStudentId(BigDecimal studentId) {
    this.studentId = studentId;
  }

  public BigDecimal getStudentId() {
    return this.studentId;
  }

  // ----------------------------------------------------------------------
  // GETTERS & SETTERS FOR FIELDS
  // ----------------------------------------------------------------------
  // --- DATABASE MAPPING : FNAME ( VARCHAR2 )
  public void setFname(String fname) {
    this.fname = fname;
  }

  public String getFname() {
    return this.fname;
  }

  // --- DATABASE MAPPING : LNAME ( VARCHAR2 )
  public void setLname(String lname) {
    this.lname = lname;
  }

  public String getLname() {
    return this.lname;
  }

  // --- DATABASE MAPPING : AGE ( NUMBER )
  public void setAge(BigDecimal age) {
    this.age = age;
  }

  public BigDecimal getAge() {
    return this.age;
  }

  // --- DATABASE MAPPING : GENDER ( VARCHAR2 )
  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getGender() {
    return this.gender;
  }

  // --- DATABASE MAPPING : CREATED_BY ( VARCHAR2 )
  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public String getCreatedBy() {
    return this.createdBy;
  }

  // --- DATABASE MAPPING : CREATED_DT ( DATE )
  @Override
  public void setCreatedDt(Date createdDt) {
    this.createdDt = createdDt;
  }

  @Override
  public Date getCreatedDt() {
    return this.createdDt;
  }

  // --- DATABASE MAPPING : LAST_UPDATED_DT ( DATE )
  @Override
  public void setLastUpdatedDt(Date lastUpdatedDt) {
    this.lastUpdatedDt = lastUpdatedDt;
  }

  @Override
  public Date getLastUpdatedDt() {
    return this.lastUpdatedDt;
  }

  // --- DATABASE MAPPING : LAST_UPDATED_BY ( VARCHAR2 )
  @Override
  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  @Override
  public String getLastUpdatedBy() {
    return this.lastUpdatedBy;
  }

  // --- DATABASE MAPPING : SERVER_NAME ( VARCHAR2 )
  @Override
  public void setServerName(String serverName) {
    this.serverName = serverName;
  }

  @Override
  public String getServerName() {
    return this.serverName;
  }

  // --- DATABASE MAPPING : SYSTEM_NAME ( VARCHAR2 )
  @Override
  public void setSystemName(String systemName) {
    this.systemName = systemName;
  }

  @Override
  public String getSystemName() {
    return this.systemName;
  }

  // --- DATABASE MAPPING : VER_NO ( NUMBER )
  @Override
  public void setVerNo(Long verNo) {
    this.verNo = verNo;
  }

  @Override
  public Long getVerNo() {
    return this.verNo;
  }

  // ----------------------------------------------------------------------
  // GETTERS & SETTERS FOR LINKS
  // ----------------------------------------------------------------------
  public void setListOfClassroomfk(List<ClassroomEntity> listOfClassroomfk) {
    this.listOfClassroomfk = listOfClassroomfk;
  }

  public List<ClassroomEntity> getListOfClassroomfk() {
    return this.listOfClassroomfk;
  }

  // ----------------------------------------------------------------------
  // toString METHOD
  // ----------------------------------------------------------------------
  @Override
  public String toString() {

    StringBuilder sb = new StringBuilder();

    sb.append("[");
    sb.append(studentId);
    sb.append("]:");
    sb.append(fname);
    sb.append("|");
    sb.append(lname);
    sb.append("|");
    sb.append(age);
    sb.append("|");
    sb.append(gender);
    sb.append("|");
    sb.append(createdBy);
    sb.append("|");
    sb.append(createdDt);
    sb.append("|");
    sb.append(lastUpdatedDt);
    sb.append("|");
    sb.append(lastUpdatedBy);
    sb.append("|");
    sb.append(serverName);
    sb.append("|");
    sb.append(systemName);
    sb.append("|");
    sb.append(verNo);

    return sb.toString();
  }

}
