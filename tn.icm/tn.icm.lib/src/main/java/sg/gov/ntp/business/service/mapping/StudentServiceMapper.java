/*
 * Created on 11 Sep 2017 ( Time 16:40:42 )
 */
package sg.gov.ntp.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import sg.gov.ntp.vo.StudentVO;
import sg.gov.ntp.vo.jpa.StudentEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class StudentServiceMapper extends AbstractServiceMapper {

	/**
	 * ModelMapper : bean to bean mapping library.
	 */
	private ModelMapper modelMapper;

	/**
	 * Constructor.
	 */
	public StudentServiceMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Mapping from 'StudentEntity' to 'Student'
	 * @param studentEntity
	 */
	public StudentVO mapStudentEntityToStudent(StudentEntity studentEntity) {

		//--- Generic mapping
		return map(studentEntity, StudentVO.class);

	}

	/**
	 * Mapping from 'Student' to 'StudentEntity'
	 * @param student
	 * @param studentEntity
	 */
	public void mapStudentToStudentEntity(StudentVO student, StudentEntity studentEntity) {

		//--- Generic mapping
		map(student, studentEntity);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ModelMapper getModelMapper() {
		return modelMapper;
	}

	protected void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}