package sg.gov.ntp.facade;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sg.gov.ntp.business.service.StudentService;
import sg.gov.ntp.common.facade.NTPBaseFacade;
import sg.gov.ntp.vo.StudentVO;

@Component
@Transactional
public class StudentFacade extends NTPBaseFacade {

  @Resource
  StudentService studentService;

  public StudentVO saveStudent(StudentVO student) {
    return studentService.create(student);
  }

  public StudentVO getStudent(BigDecimal studentId) {
    return studentService.findById(studentId);
  }

}
