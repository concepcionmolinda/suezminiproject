package sg.gov.ntp.controller;

/**
 * This is a Controller
 */

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sg.gov.ntp.common.controller.NTPBaseController;
import sg.gov.ntp.facade.StudentFacade;
import sg.gov.ntp.vo.StudentVO;

@Controller
public class StudentController extends NTPBaseController {

  @Resource
  StudentFacade studentFacade;

  @RequestMapping(value = "/ntp/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<String> saveStudent(@RequestBody StudentVO student) {

    studentFacade.saveStudent(student);
    return new ResponseEntity<String>("Record successfully saved.",
        HttpStatus.OK);

  }

  @RequestMapping(value = "/ntp/student/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<StudentVO> getStudent(
      @PathVariable("id") BigDecimal studentId) {

    StudentVO student = studentFacade.getStudent(studentId);
    return new ResponseEntity<StudentVO>(student, HttpStatus.OK);

  }

}