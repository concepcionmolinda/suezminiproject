package sg.gov.ntp.facade;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import sg.gov.ntp.business.service.StudentService;
import sg.gov.ntp.vo.StudentVO;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class StudentFacadeTest {

  @Mock
  StudentService studentService;

  @InjectMocks
  StudentFacade studentFacade;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void saveStudent() throws Exception {

    StudentVO studentVO = new StudentVO();

    studentVO.setStudentId(new BigDecimal(4));
    studentVO.setFname("nikki");
    studentVO.setLname("kho");
    studentVO.setAge(new BigDecimal(21));
    studentVO.setGender("f");
    studentVO.setCreatedBy("admin");
    studentVO.setCreatedDt(null);
    studentVO.setLastUpdatedDt(null);
    studentVO.setLastUpdatedBy("admin");
    studentVO.setServerName("xe");
    studentVO.setSystemName("suezminiproject");
    studentVO.setVerNo(new Long(1));

    studentFacade.saveStudent(studentVO);

    Mockito.when(studentService.create(Mockito.any(StudentVO.class)))
        .thenReturn(studentVO);

    studentFacade.saveStudent(Mockito.any(StudentVO.class));
    Mockito.verify(studentService).create(Mockito.any(StudentVO.class));
  }

  @Test
  public void getStudent() throws Exception {

    StudentVO studentVO = new StudentVO();

    studentVO.setStudentId(new BigDecimal(4));
    studentVO.setFname("nikki");
    studentVO.setLname("kho");
    studentVO.setAge(new BigDecimal(21));
    studentVO.setGender("f");
    studentVO.setCreatedBy("admin");
    studentVO.setCreatedDt(null);
    studentVO.setLastUpdatedDt(null);
    studentVO.setLastUpdatedBy("admin");
    studentVO.setServerName("xe");
    studentVO.setSystemName("suezminiproject");
    studentVO.setVerNo(new Long(1));

    studentFacade.getStudent(new BigDecimal(4));

    Mockito.when(studentService.findById(Mockito.any(BigDecimal.class)))
        .thenReturn(studentVO);

    studentFacade.getStudent(Mockito.any(BigDecimal.class));
    Mockito.verify(studentService).findById(Mockito.any(BigDecimal.class));
  }

}
