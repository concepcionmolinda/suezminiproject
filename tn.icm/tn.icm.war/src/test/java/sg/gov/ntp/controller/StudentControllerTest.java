package sg.gov.ntp.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import net.minidev.json.JSONObject;
import sg.gov.ntp.facade.StudentFacade;
import sg.gov.ntp.vo.StudentVO;

@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class StudentControllerTest {

  @Mock
  StudentFacade studentFacade;

  @InjectMocks
  StudentController studentController;

  private MockMvc mockMvc;

  @Before
  public void setup() throws Exception {
    MockitoAnnotations.initMocks(this);
    this.mockMvc = MockMvcBuilders.standaloneSetup(studentController).build();
  }

  @Test
  public void saveStudent() throws Exception {

    Mockito.when(studentFacade.saveStudent(Mockito.any(StudentVO.class)))
        .thenReturn(new StudentVO());

    JSONObject jsonObject = new JSONObject();

    jsonObject.put("studentId", 8);
    jsonObject.put("fname", "nikki");
    jsonObject.put("lname", "lha");
    jsonObject.put("age", 25);
    jsonObject.put("gender", "f");
    jsonObject.put("createdBy", "admin");
    jsonObject.put("createdDt", null);
    jsonObject.put("lastUpdatedDt", null);
    jsonObject.put("lastUpdatedBy", "admin");
    jsonObject.put("serverName", "xe");
    jsonObject.put("systemName", "suezminiproject");
    jsonObject.put("verNo", 1);

    System.out.println(jsonObject.toString());
    MvcResult result = this.mockMvc
        .perform(post("/ntp/save").content(jsonObject.toJSONString())
            .header("transactionId", "1").header("sessionId", "1")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON))
        .andDo(print()).andExpect(status().isOk()).andReturn();

    assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
    Mockito.verify(studentFacade).saveStudent(Mockito.any(StudentVO.class));
  }

  @Test
  public void getStudent() throws Exception {

    Mockito.when(studentFacade.getStudent(Mockito.any(BigDecimal.class)))
        .thenReturn(new StudentVO());

    JSONObject jsonObject = new JSONObject();

    jsonObject.put("studentId", 5);
    jsonObject.put("fname", "nikki");
    jsonObject.put("lname", "lha");
    jsonObject.put("age", 25);
    jsonObject.put("gender", "f");
    jsonObject.put("createdBy", "admin");
    jsonObject.put("createdDt", null);
    jsonObject.put("lastUpdatedDt", null);
    jsonObject.put("lastUpdatedBy", "admin");
    jsonObject.put("serverName", "xe");
    jsonObject.put("systemName", "suezminiproject");
    jsonObject.put("verNo", 1);

    MvcResult result = this.mockMvc
        .perform(get("/ntp/student/5").content(jsonObject.toJSONString())
            .header("transactionId", "1").header("sessionId", "1")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON))
        .andDo(print()).andExpect(status().isOk()).andReturn();

    assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
    Mockito.verify(studentFacade).getStudent(Mockito.any(BigDecimal.class));
  }

}
